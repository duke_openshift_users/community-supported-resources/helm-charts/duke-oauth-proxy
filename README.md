# A simple [oauth2-proxy](https://oauth2-proxy.github.io/oauth2-proxy/) in front of a whoami container

This is just a simple chart that deploys the oauth2-proxy and is sorta preconfigured to work against the [OIT IDMS OIDC](https://authentication.oit.duke.edu/manager/oauth) service.

local prereqs:

- [helm](https://helm.sh)
- [helmfile](https://github.com/roboll/helmfile)

(Both should also be available in brew)

deploy with:

```bash
export OIDC_CLIENT_ID=your-client-id
export OIDC_CLIENT_SECRET=your-client-secret
export ROUTE_HOSTNAME=funky-release-name-something.somewhere.duke.edu
helmfile apply
```

or:

```bash
helm install funky-release-name --set oidc.client.id=${OIDC_CLIENT_ID},oidc.client.secret=${OIDC_CLIENT_SECRET},routeHostname=${ROUTE_HOSTNAME} .
```

The `upstream.container` value can be set to a [container item](https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/#Container) inside a kubernetes pod spec. The `upstream.port` value should be set to the port exposed in the defined container. Below is the default and a decent example:

```yaml
upstream:
  container: |-
    - image: image-mirror-prod-registry.cloud.duke.edu/traefik/whoami:latest
      name: whoami
      ports:
      - containerPort: 8080
      args:
        - --port
        - "8080"
      resources: {}
```

A list of permitted groups can be set into the `oidc.client.allowedGroups` value to restrict the proxied service to a specific [Policy Group](https://groups.oit.duke.edu/groupmanager/dukeGroups/policyGroups).

Robots lovingly delivered by robohash.org
